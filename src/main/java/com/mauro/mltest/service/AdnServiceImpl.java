package com.mauro.mltest.service;

import java.text.DecimalFormat;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mauro.mltest.dto.AdnDTO;
import com.mauro.mltest.dto.StatsDTO;
import com.mauro.mltest.entity.AdnEntity;
import com.mauro.mltest.repository.AdnRepository;
import com.mauro.mltest.util.ValidateAdnUtil;

/**
 * AdnService implementation.
 * 
 * @author Mauro Baioni <mdbaioni@gmail.com>
 */
@Service
public class AdnServiceImpl implements AdnService {

	@Autowired
	AdnRepository repository;

	/**
	 * Receives an AdnDTO and maps it to an entity to save in database.
	 * 
	 * @param AdnDTO 
	 */
	@Override
	public void saveADN(AdnDTO dto) {

		dto.setIsMutant(ValidateAdnUtil.isMutant(dto.getAdn()));

		AdnEntity entity = new AdnEntity();
		entity.setAdn(new Gson().toJson(dto.getAdn()));
		entity.setIsMutant(dto.getIsMutant());
		try {
			repository.save(entity);
		} catch (DataAccessException dae) {
			dae.printStackTrace();
		} catch(EntityNotFoundException enfe) {
			enfe.printStackTrace();
		}

	}

	/**
	 * Returns mutant and humans stats.
	 * 
	 * @return String
	 */
	@Override
	public String getStats() {

		StatsDTO stats = null;

		Double ratio = 0D;
		try {
			stats = new StatsDTO();
			stats.setMutants(repository.countMutants());
			stats.setHumans(repository.countNotMutants());

			if (stats.getMutants() > 0 && stats.getHumans() > 0) {
				ratio = stats.getMutants().doubleValue() / stats.getHumans();
				stats.setRatio(new DecimalFormat("#.##").format(ratio));
			} else {
				stats.setRatio("0");
			}
		} catch (DataAccessException dae) {
			dae.printStackTrace();
		} catch(EntityNotFoundException enfe) {
			enfe.printStackTrace();
		}

		return (stats != null) ? new Gson().toJson(stats) : "";
	}

}
