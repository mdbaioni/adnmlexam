package com.mauro.mltest.service;

import com.mauro.mltest.dto.AdnDTO;

/**
 * 
 * @author Mauro Baioni <mdbaioni@gmail.com>
 */
public interface AdnService {
	
	void saveADN(AdnDTO dto);
	
	String getStats();

}
