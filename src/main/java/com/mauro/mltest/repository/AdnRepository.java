package com.mauro.mltest.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.mauro.mltest.entity.AdnEntity;

/**
 * This interface can perform Spring Data CRUD operations.
 * 
 * @author Mauro Baioni <mdbaioni@gmail.com>
 */
public interface AdnRepository extends CrudRepository<AdnEntity, Long> {

	/**
	 * Returns mutants quantity stored in database.
	 * 
	 * @return Long
	 */
	@Query("SELECT COUNT (a) From AdnEntity a WHERE a.isMutant=true")
	Long countMutants();

	/**
	 * Returns humans quantity stored in database.
	 * 
	 * @return Long
	 */
	@Query("SELECT COUNT (a) From AdnEntity a WHERE a.isMutant=false")
	Long countNotMutants();

	/**
	 * Returns table rows count.
	 * 
	 * @return Long
	 */
	@Query("SELECT COUNT (a) From AdnEntity a")
	Long countAll();
}
