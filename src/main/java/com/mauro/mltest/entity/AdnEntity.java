package com.mauro.mltest.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity class to map adn table.
 * 
 * @author Mauro Baioni <mdbaioni@gmail.com>
 */
@Entity
@Table(name = "adn", schema = "public")
public class AdnEntity {

	@Id
	@GeneratedValue
	private Integer id;

	@Column(name = "adn_string")
	private String adn;

	@Column(name = "isMutant")
	private Boolean isMutant;

	/**
	 * 
	 * @return adn string.
	 */
	public String getAdn() {
		return adn;
	}

	/**
	 * 
	 * @param adn string.
	 */
	public void setAdn(String adn) {
		this.adn = adn;
	}

	/**
	 * Returns isMutant state.
	 * 
	 * @return isMutant
	 */
	public Boolean getIsMutant() {
		return isMutant;
	}

	/**
	 * 
	 * @param isMutant
	 */
	public void setIsMutant(Boolean isMutant) {
		this.isMutant = isMutant;
	}

}
