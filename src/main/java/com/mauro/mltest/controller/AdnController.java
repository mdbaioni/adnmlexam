package com.mauro.mltest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mauro.mltest.dto.AdnDTO;
import com.mauro.mltest.service.AdnService;
import com.mauro.mltest.util.ValidateAdnUtil;

/**
 * HTTP methods handler.
 * 
 * @author Mauro Baioni <mdbaioni@gmail.com>
 */
@RestController
public class AdnController {

	//Service injection.
	@Autowired
	AdnService service;

	/**
	 * Returns an HttpStatus depending on adn sequence check. 
	 * @param Json obect wrapped by AdnDTO
	 * @return HttpStatus
	 */
	@RequestMapping(value = "/mutant", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public HttpStatus mutant(@RequestBody AdnDTO dto) {

		if (!ValidateAdnUtil.validateMutantChars(dto.getAdn())) {
			return HttpStatus.BAD_REQUEST;
		} else {

			service.saveADN(dto);

			return ValidateAdnUtil.isMutant(dto.getAdn()) ? HttpStatus.OK : HttpStatus.FORBIDDEN;
		}
	}

	/**
	 * Returns a Json Object descripting mutants and humans stats.
	 * 
	 * @return Json Object
	 */
	@RequestMapping(value = "/stats", method = RequestMethod.GET)
	@ResponseBody
	public String stats() {
		return service.getStats();
	}
}
