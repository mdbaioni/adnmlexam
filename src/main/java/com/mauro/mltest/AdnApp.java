package com.mauro.mltest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author Mauro Baioni <mdbaioni@gmail.com>
 *
 * Class to initialize spring boot application.
 *
 */
@SpringBootApplication
public class AdnApp {

	public static void main(String[] args) {
		SpringApplication.run(AdnApp.class, args);
	}
}
