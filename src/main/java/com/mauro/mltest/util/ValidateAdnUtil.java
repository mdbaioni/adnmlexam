package com.mauro.mltest.util;

import java.util.regex.Pattern;

/**
 * Tests mutant detection algorithms.
 * 
 * @author Mauro Baioni <mdbaioni@gmail.com>
 */
public class ValidateAdnUtil {

	private final static String MUTANT_SECUENCES = "AAAA|CCCC|TTTT|GGGG";

	/**
	 * Test adn sequences.
	 * 
	 * @param adn
	 * @return boolean
	 */
	public static boolean isMutant(String[] adn) {

		Pattern pattern = Pattern.compile(MUTANT_SECUENCES);

		for (int i = 0; i < adn.length; i++) {
			adn[i] = adn[i].toUpperCase().replaceAll("\\s", "");
		}

		// Horizontales.
		int horizontals = 0;
		for (String str : adn) {
			if (pattern.matcher(str).find())
				horizontals++;
		}

		// Verticales.
		int verticals = 0;
		for (int i = 0; i < adn.length; i++) {
			StringBuilder stb = new StringBuilder();
			for (int j = 0; j < adn.length; j++) {
				stb.append(adn[j].charAt(i));
			}
			if (pattern.matcher(stb.toString()).find())
				verticals++;
		}

		// Diagonals
		int diagonals = 0;
		for (int i = 0; i < adn.length - 3; i++) {
			for (int j = 0; j < adn.length - 3; j++) {
				StringBuilder diagonalBuilder = new StringBuilder();
				diagonalBuilder.append(adn[i].charAt(j));
				diagonalBuilder.append(adn[i + 1].charAt(j + 1));
				diagonalBuilder.append(adn[i + 2].charAt(j + 2));
				diagonalBuilder.append(adn[i + 3].charAt(j + 3));

				if (pattern.matcher(diagonalBuilder.toString()).find())
					diagonals++;
			}
		}

		return (horizontals + verticals + diagonals) > 1 ? true : false;
	}

	/**
	 * Test adn valid chars.
	 * 
	 * @param adn
	 * @return boolean
	 */
	public static Boolean validateMutantChars(String[] adn) {

		for (int i = 0; i < adn.length; i++) {
			if (Pattern.compile("[^AaCcGgTt]").matcher(adn[i]).find())
				return false;
		}

		return true;
	}
}
