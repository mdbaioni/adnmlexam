package com.mauro.mltest.dto;

/**
 * Class to wrap adn string and its isMutant state.
 * 
 * @author Mauro Baioni <mdbaioni@gmail.com>
 */
public class AdnDTO {

	String[] adn;
	Boolean isMutant;

	public String[] getAdn() {
		return adn;
	}

	/**
	 * 
	 * @param adn
	 */
	public void setAdn(String[] adn) {
		this.adn = adn;
	}

	/**
	 * 
	 * @return isMutant
	 */
	public Boolean getIsMutant() {
		return isMutant;
	}

	/**
	 * 
	 * @param isMutant
	 */
	public void setIsMutant(Boolean isMutant) {
		this.isMutant = isMutant;
	}

}
