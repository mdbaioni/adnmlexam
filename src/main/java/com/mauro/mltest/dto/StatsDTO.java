package com.mauro.mltest.dto;

/**
 * Wraps stats data. 
 * 
 * @author Mauro Baioni <mdbaioni@gmail.com>
 */
public class StatsDTO {

	private Long mutants;
	private Long humans;
	private String ratio;

	/**
	 * 
	 * @return Long
	 */
	public Long getMutants() {
		return mutants;
	}

	/**
	 * 
	 * @param Long
	 */
	public void setMutants(Long mutants) {
		this.mutants = mutants;
	}

	/**
	 * 
	 * @return Long
	 */
	public Long getHumans() {
		return humans;
	}

	/**
	 * 
	 * @param Long
	 */
	public void setHumans(Long humans) {
		this.humans = humans;
	}

	/**
	 * 
	 * @return Long
	 */
	public String getRatio() {
		return ratio;
	}

	/**
	 * 
	 * @param Long
	 */
	public void setRatio(String ratio) {
		this.ratio = ratio;
	}

}
