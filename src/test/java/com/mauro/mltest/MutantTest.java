package com.mauro.mltest;

import org.junit.Assert;
import org.junit.Test;

import com.mauro.mltest.util.ValidateAdnUtil;

public class MutantTest {

	@Test
	public void isMutantTest() {
		String[] mutantAdn = {"ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"};

		Assert.assertTrue(ValidateAdnUtil.isMutant(mutantAdn));

	}
	
	@Test
	public void notMutantTest() {
		String[] notMutantAdn = {"ATGCGA", "CAGTGC", "TTCTGT", "AGAAGG", "CCCGTA", "TCACTG"};
		Assert.assertFalse(ValidateAdnUtil.isMutant(notMutantAdn));
	}
	
	@Test
	public void validMutantCharsTest() {
		String[] validChars = {"ATGCGA", "CAGTGC", "TTCTGT", "AGAAGG", "CCCGTA", "TCACTG"};
		Assert.assertTrue(ValidateAdnUtil.validateMutantChars(validChars));
	}
	
	@Test
	public void invalidMutantCharsTest() {
		String[] validChars = {"RTGCGA", "CAGTGC", "TTCTGT", "AGAAGG", "CCCGTA", "TCACTG"};
		Assert.assertFalse(ValidateAdnUtil.validateMutantChars(validChars));
	}

}
