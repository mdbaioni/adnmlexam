# DNATest

El proyecto consiste en recibir un Json con una secuencia de caracteres emulando un ADN.
Si se determina que el ADN es mutante, se devuelve un Status OK, caso contrario, Forbidden.
En caso de contener caracteres no válidos, se devuelve un Status BAD_REQUEST.


### Requisitos

- Motor de base de datos PostgresQL versión 9 o posterior.
- Maven versión 3 o posterior.
- JDK versión 1.8.
- Git.


### Configuración

- Clonar el proyecto. 
```
git clone https://gitlab.com/mdbaioni/adnml.git
```
- Debe crearse una base de datos en PostgresQL con nombre "adn_ml_local".
- Debe usarse un usuario de base de datos con permisos de escritura y lecutra. Puede configurar el usuario y contraseña en el archivo application.properties ubicado en src/main/resources.
- Posicionarse en el directorio del proyecto y correr ```mvn clean install``` desde una terminal para descargar todas sus dependencias.


### Ejecución
- Posicionarse en el directorio del proyecto y ejecutar ```mvn spring-boot:run``` desde una terminal.
- También puede correr la aplicación desde su ide de preferencia.
